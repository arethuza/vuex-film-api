import Vue from 'vue';
import App from './App.vue';
import store from './store'
import router from './routes';
import './assets/css/style.css'

new Vue({ 
	el: '#app', 
	router, 
	store,
	render: h => h(App) 
})