import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './pages/Dashboard.vue';
import Detail from './pages/DetailFilm.vue';

Vue.use(Router)

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			component: Dashboard
		},
		{
			path: '/detail/:id',
			name: 'detail',
			component: Detail,
		},
	]
})